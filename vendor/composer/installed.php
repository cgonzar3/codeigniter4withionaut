<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'jose/tienda-jose',
        'dev' => true,
    ),
    'versions' => array(
        'benedmunds/codeigniter-ion-auth' => array(
            'pretty_version' => '4.x-dev',
            'version' => '4.9999999.9999999.9999999-dev',
            'type' => 'library',
            'install_path' => __DIR__ . '/../benedmunds/codeigniter-ion-auth',
            'aliases' => array(),
            'reference' => '19785a229677fbaeb5886783c30f820dba851d82',
            'dev_requirement' => false,
        ),
        'jose/tienda-jose' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
